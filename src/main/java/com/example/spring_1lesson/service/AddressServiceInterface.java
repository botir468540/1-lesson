package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Address;
import com.example.spring_1lesson.payload.AddressDTO;
import com.example.spring_1lesson.payload.ApiResponse;

import java.util.List;

public interface AddressServiceInterface {

    public List<Address> getAllAddresses();

    public Address getOne(Integer id);

    public ApiResponse addAddress(AddressDTO address);

    public Address editOne(Address address);

    public ApiResponse deleteOne(Integer id);

}
