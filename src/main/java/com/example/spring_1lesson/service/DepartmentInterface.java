package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Department;
import com.example.spring_1lesson.entity.Worker;
import com.example.spring_1lesson.payload.ApiResponse;
import com.example.spring_1lesson.payload.DepartmentDTO;

import java.util.List;

public interface DepartmentInterface {

    public List<Department> getAllDepartment();

    public Department getOne(Integer id);

    public ApiResponse addDepartment(DepartmentDTO address);

    public Department editOne(Department address);

    public ApiResponse deleteOne(Integer id);
}
