package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Address;
import com.example.spring_1lesson.entity.Company;
import com.example.spring_1lesson.payload.ApiResponse;
import com.example.spring_1lesson.payload.CompanyDTO;
import com.example.spring_1lesson.repository.AddressRepository;
import com.example.spring_1lesson.repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;


@Service
public class CompanyService implements CompanyInterface{

    private final CompanyRepository repository;
    private final AddressRepository addressRepository;

    public CompanyService(CompanyRepository repository, AddressRepository addressRepository) {
        this.repository = repository;
        this.addressRepository = addressRepository;
    }

    @Override
    public List<Company> getAllCompany() {
        List<Company> allAddresses = repository.findAll();
        return allAddresses;
    }

    @Override
    public Company getOne(Integer id) {
        Optional<Company> byId = repository.findById(id);

        if (byId.isPresent()) {
            Company company = byId.get();
            return company;
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "" +
                    "Missing the required parameter ID");
        }
    }

    @Override
    public ApiResponse addCompany(CompanyDTO companyDTO) {
        Optional<Address> byId=addressRepository.findById(companyDTO.getAddress());
        if(byId.isPresent()){
            Address address=byId.get();
            Company company=new Company();
            company.setCorpName(companyDTO.getCorpName());
            company.setDirectorName(companyDTO.getDirectorName());
            company.setAddress(address);
            repository.save(company);
            return new ApiResponse("Company saved",true);
        }else
            return new ApiResponse("No such Address",false);
    }

    @Override
    public Company editOne(Company company) {
        Optional<Company> byId = Optional.empty();
        if (company.getId() != null) {
            byId = repository.findById(company.getId());
        }

        Company address1 = byId.get();
        address1.setDirectorName(company.getDirectorName());
        address1.setCorpName(company.getCorpName());
        address1.setAddress(company.getAddress());
        Company save=repository.save(address1);


        return save;
    }

    @Override
    public ApiResponse deleteOne(Integer id) {
        Optional<Company> byId = repository.findById(id);

        if(byId.isPresent()){
            repository.deleteById(id);
            return new ApiResponse("Item deleted",true);
        }else{
            return new ApiResponse("No such company",false);

        }

    }
}
