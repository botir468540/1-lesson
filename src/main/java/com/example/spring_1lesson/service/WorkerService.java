package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Address;
import com.example.spring_1lesson.entity.Company;
import com.example.spring_1lesson.entity.Department;
import com.example.spring_1lesson.entity.Worker;
import com.example.spring_1lesson.payload.ApiResponse;
import com.example.spring_1lesson.payload.WorkerDTO;
import com.example.spring_1lesson.repository.AddressRepository;
import com.example.spring_1lesson.repository.DepartmentRepository;
import com.example.spring_1lesson.repository.WorkerRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;

@Service
public class WorkerService implements WorkerInterface {
    private final WorkerRepository repository;

    private final AddressRepository addressRepository;

    private final DepartmentRepository departmentRepository;

    public WorkerService(WorkerRepository repository, AddressRepository addressRepository, DepartmentRepository departmentRepository) {
        this.repository = repository;
        this.addressRepository = addressRepository;
        this.departmentRepository = departmentRepository;
    }


    @Override
    public List<Worker> getAllWorker() {
        List<Worker> allWorkers = repository.findAll();
        return allWorkers;
    }

    @Override
    public Worker getOne(Integer id) {
        Optional<Worker> byId = repository.findById(id);

        if (byId.isPresent()) {
            Worker worker = byId.get();
            return worker;
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "" +
                    "Missing the required parameter ID");
        }
    }

    @Override
    public ApiResponse addWorker(WorkerDTO workerDTO) {
        Optional<Address> byId = addressRepository.findById(workerDTO.getAddressId());
        Optional<Department> byIdDepartment = departmentRepository.findById(workerDTO.getDepartmentId());
        if (byId.isPresent() && byIdDepartment.isPresent()) {
            Address address = byId.get();
            Department department =byIdDepartment.get();
            Worker worker =new Worker();

            worker.setName(workerDTO.getName());
            worker.setPhoneNumber(workerDTO.getPhoneNumber());
            worker.setAddress(address);
            worker.setDepartment(department);
            repository.save(worker);
            return new ApiResponse("DepartmentDTO saved", true);
        } else
            return new ApiResponse("No such Company", false);
    }

    @Override
    public Worker editOne(Worker worker) {
        Optional<Worker> byId = Optional.empty();
        if (worker.getId() != null) {
            byId = repository.findById(worker.getId());
        }

        Worker address1 = byId.get();
        address1.setName(worker.getName());
        address1.setPhoneNumber(worker.getPhoneNumber());
        address1.setAddress(worker.getAddress());
        address1.setDepartment(worker.getDepartment());
        Worker save = repository.save(address1);


        return save;
    }

    @Override
    public ApiResponse deleteOne(Integer id) {
        Optional<Worker> byId = repository.findById(id);

        if (byId.isPresent()) {
            repository.deleteById(id);
            return new ApiResponse("Item deleted",true);
        } else {
            return new ApiResponse("no such worker deleted",true);
        }

    }
}
