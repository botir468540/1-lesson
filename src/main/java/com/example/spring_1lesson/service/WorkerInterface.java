package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Company;
import com.example.spring_1lesson.entity.Worker;
import com.example.spring_1lesson.payload.ApiResponse;
import com.example.spring_1lesson.payload.WorkerDTO;

import java.util.List;

public interface WorkerInterface {

    public List<Worker> getAllWorker();

    public Worker getOne(Integer id);

    public ApiResponse addWorker(WorkerDTO workerDTO);

    public Worker editOne(Worker worker);

    public ApiResponse deleteOne(Integer id);
}
