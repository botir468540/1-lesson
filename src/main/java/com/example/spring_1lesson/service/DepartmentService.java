package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Address;
import com.example.spring_1lesson.entity.Company;
import com.example.spring_1lesson.entity.Department;
import com.example.spring_1lesson.payload.ApiResponse;
import com.example.spring_1lesson.payload.DepartmentDTO;
import com.example.spring_1lesson.repository.CompanyRepository;
import com.example.spring_1lesson.repository.DepartmentRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.Optional;
@Service
public class DepartmentService implements DepartmentInterface {

    private final DepartmentRepository repository;

    private final CompanyRepository companyRepository;

    public DepartmentService(DepartmentRepository repository, CompanyRepository companyRepository) {
        this.repository = repository;
        this.companyRepository = companyRepository;
    }

    @Override
    public List<Department> getAllDepartment() {
        List<Department> allAddresses = repository.findAll();
        return allAddresses;
    }

    @Override
    public Department getOne(Integer id) {
        Optional<Department> byId = repository.findById(id);

        if (byId.isPresent()) {
            Department company = byId.get();
            return company;
        } else {
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST, "" +
                    "Missing the required parameter ID");
        }
    }

    @Override
    public ApiResponse addDepartment(DepartmentDTO departmentDTO) {
        Optional<Company> byId=companyRepository.findById(departmentDTO.getCompanyId());
        if(byId.isPresent()){
            Company company=byId.get();
            Department department=new Department();
            department.setName(departmentDTO.getName());
            department.setCompany(company);
            repository.save(department);
            return new ApiResponse("DepartmentDTO saved",true);
        }else
            return new ApiResponse("No such Company",false);
    }

    @Override
    public Department editOne(Department department) {
        Optional<Department> byId = Optional.empty();
        if (department.getId() != null) {
            byId = repository.findById(department.getId());
        }

        Department address1 = byId.get();
        address1.setName(department.getName());
        address1.setCompany(department.getCompany());
        Department save=repository.save(address1);


        return save;
    }

    @Override
    public ApiResponse deleteOne(Integer id) {
        Optional<Department> byId = repository.findById(id);

        if(byId.isPresent()){
            repository.deleteById(id);
            return new ApiResponse("Item deleted",true);
        }else{
            return new ApiResponse("No such Department",false);
        }
    }
}
