package com.example.spring_1lesson.service;

import com.example.spring_1lesson.entity.Address;
import com.example.spring_1lesson.entity.Company;
import com.example.spring_1lesson.payload.AddressDTO;
import com.example.spring_1lesson.payload.ApiResponse;
import com.example.spring_1lesson.payload.CompanyDTO;

import java.util.List;

public interface CompanyInterface {

    public List<Company> getAllCompany();

    public Company getOne(Integer id);

    public ApiResponse addCompany(CompanyDTO companyDTO);

    public Company editOne(Company address);

    public ApiResponse deleteOne(Integer id);
}
