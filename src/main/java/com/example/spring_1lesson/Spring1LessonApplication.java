package com.example.spring_1lesson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Spring1LessonApplication {

    public static void main(String[] args) {
        SpringApplication.run(Spring1LessonApplication.class, args);
    }

}
