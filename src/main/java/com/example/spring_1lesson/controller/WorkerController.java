package com.example.spring_1lesson.controller;

import com.example.spring_1lesson.entity.Department;
import com.example.spring_1lesson.entity.Worker;
import com.example.spring_1lesson.payload.WorkerDTO;
import com.example.spring_1lesson.service.WorkerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/worker")
public class WorkerController {

    private final WorkerService service;

    public WorkerController(WorkerService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllDepartment(){
        return ResponseEntity.ok().body(service.getAllWorker());
    }

    @PostMapping
    public ResponseEntity<?> addDepartment(@RequestBody WorkerDTO workerDTO){
        return ResponseEntity.ok().body(service.addWorker(workerDTO));
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getOneDepartment(@PathVariable(name = "id") Integer id){
        return ResponseEntity.ok().body(service.getOne(id));
    }

    @PutMapping
    public ResponseEntity<?> updateDepartment(@RequestBody Worker company){
        return ResponseEntity.ok().body(service.editOne(company));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteOne(@PathVariable(name ="id") Integer id){
        return ResponseEntity.ok().body(service.deleteOne(id));
    }

}
