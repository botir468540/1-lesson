package com.example.spring_1lesson.controller;


import com.example.spring_1lesson.entity.Company;
import com.example.spring_1lesson.entity.Department;
import com.example.spring_1lesson.payload.DepartmentDTO;
import com.example.spring_1lesson.service.DepartmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/department")
public class DepartmentController {

    private final DepartmentService service;

    public DepartmentController(DepartmentService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getAllDepartment(){
        return ResponseEntity.ok().body(service.getAllDepartment());
    }

    @PostMapping
    public ResponseEntity<?> addDepartment(@RequestBody DepartmentDTO departmentDTO){
        return ResponseEntity.ok().body(service.addDepartment(departmentDTO));
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getOneDepartment(@PathVariable(name = "id") Integer id){
        return ResponseEntity.ok().body(service.getOne(id));
    }

    @PutMapping
    public ResponseEntity<?> updateDepartment(@RequestBody Department company){
        return ResponseEntity.ok().body(service.editOne(company));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteOne(@PathVariable(name ="id") Integer id){
        return ResponseEntity.ok().body(service.deleteOne(id));
    }
}
