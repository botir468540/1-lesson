package com.example.spring_1lesson.payload;

public class AddressDTO {

    private String street;

    private String homeNumber;

    public AddressDTO() {
    }


    public AddressDTO(String street, String homeNumber) {
        this.street = street;
        this.homeNumber = homeNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }
}
