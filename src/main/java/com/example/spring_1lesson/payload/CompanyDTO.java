package com.example.spring_1lesson.payload;

import java.util.List;

public class CompanyDTO {

    private String corpName;

    private String directorName;

    private Integer address;

    public CompanyDTO() {
    }

    public CompanyDTO(String corpName, String directorName, Integer address) {
        this.corpName = corpName;
        this.directorName = directorName;
        this.address = address;
    }

    public String getCorpName() {
        return corpName;
    }

    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public Integer getAddress() {
        return address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }
}
