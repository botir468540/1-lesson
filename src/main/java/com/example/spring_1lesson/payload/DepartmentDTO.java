package com.example.spring_1lesson.payload;

public class DepartmentDTO {

    private String name;

    private Integer companyId;

    public DepartmentDTO(String name, Integer companyId) {
        this.name = name;
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }
}
