package com.example.spring_1lesson.payload;

public class WorkerDTO {

    private String name;

    private String phoneNumber;

    private Integer addressId;

    private Integer departmentId;

    public WorkerDTO() {
    }

    public WorkerDTO(String name, String phoneNumber, Integer addressId, Integer departmentId) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.addressId = addressId;
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }
}
